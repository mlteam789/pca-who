# -*- coding: utf-8 -*-
"""	
Created on Tue Jan 17 13:41:10 2017

"""
import numpy as np
from collections import Counter

class NearestNeighbor(object):
    #http://cs231n.github.io/classification/
    def __init__(self):
        pass

    def train(self, X, y):
        """ X is N x D where each row is an example. Y is 1-dimension of size N """
        # the nearest neighbor classifier simply remembers all the training data
        self.Xtr = X
        self.ytr = y

    def predict(self, X, l='L1', k=1):
        """ X is N x D where each row is an example we wish to predict label for """
        num_test = X.shape[0]
        yClose = [] 
	#Evaluate the Label according the nearest K labels: through pushing them into list
        # lets make sure that the output type matches the input type
        Ypred = np.zeros(num_test, dtype = self.ytr.dtype)
        # loop over all test rows
        for i in range(num_test):
            # find the nearest training example to the i'th test example
            if l == 'L1':
                # using the L1 distance (sum of absolute value differences)
                distances = np.sum(np.abs(self.Xtr - X[i,:]), axis = 1)
            else:
                distances = np.sqrt(np.sum(np.square(self.Xtr - X[i,:]), axis = 1))
                # Find the k nearest neighbors and extract their labels
	        #Retrieve an array containing the sort of distances by their indices
            iClose = np.argsort(distances)
	         #Take the first K indcies in the array and list them
            iClose = iClose[:k].tolist()
            #Extract the labels referred by the selected indices and but them into list 
            yClose = self.ytr[iClose]
            # evaluate the frequency among the k closests neighbor then assign it
            Ypred[i] = Counter(yClose).most_common(1)[0][0] # predict the label as the most frequent item label

        return Ypred
