import csv
import numpy as np
from sklearn.model_selection import StratifiedKFold
import matplotlib.pyplot as plt
import NearestNeighbor as NN
import numpy as np
import pandas as pd

class PCA:
    u,mean = None,None

    #Return the projection of train data
    def train(x,n):
        PCA.mean = np.mean(x, axis = 0)
        x = x - PCA.mean
        cov = np.dot(x.T, x) / x.shape[0]
        PCA.u,s,v = np.linalg.svd(cov)

        PCA.u = PCA.u[:,:n]
        keptVar = s[:n].sum()/s.sum() * 100

        return np.dot(x,PCA.u), keptVar
    
    #Return the projection of test data
    def test(test):
        test = test - PCA.mean;
        return np.dot(test,PCA.u)

def KNNmodel(x_train,y_train,x_test,y_test):
    # Create a NN classifier instance and train it with training data
    classifier = NN.NearestNeighbor()
    # train k-nearest neighbor classifier
    classifier.train(x_train, y_train)
    # I use the K value of 50 with L1 one distance
    yPred = classifier.predict(x_test,'L1', k=50)
    # Compute and print the fraction of correctly predicted examples
    totCorrect = np.sum(y_test == yPred)
    totAcc = float(totCorrect) / x_test.shape[0]
    return totAcc * 100;

seed = 1337
np.random.seed(seed)
kfold = StratifiedKFold(n_splits=5,shuffle=False,
    random_state=seed)

#Uplead the data from .csv file
data = np.ones(shape=(768,8))
label = np.ones(shape=(768,))
with open('pima-indians-diabetes.csv') as f:
    readIn = csv.reader(f, delimiter=',')
    for i,row in enumerate(readIn):
        data[i,:] = row[:8]
        label[i] = row[8]
print(label[:10])

#Evaluate With PCA
var = []
crossVal_scores = []
usedDim = []
for n in range(1,9):
    for train, test in kfold.split(data, label):
        x_train,keptVar = PCA.train(data[train],n)
        y_train = label[train]
        x_test = PCA.test(data[test])
        y_test = label[test]

        var.append(keptVar)
        crossVal_scores.append(KNNmodel(x_train,y_train,x_test,y_test))
        usedDim.append(n)

        
plt.ylabel('PCA Kept Variance')
plt.xlabel('num of eigenvectors')
plt.plot(usedDim,var,'ro')
plt.show()

PCAscores = []
while len(crossVal_scores) > 0:
    PCAscores.append(sum(crossVal_scores[:5])/5)
    del crossVal_scores[:5]

print ("-----------------PCA-----------------")
print(PCAscores)
plt.ylabel('average cross validation accuracy')
plt.xlabel('num of eigvectors')
plt.plot(range(1,9),PCAscores,'ro')
plt.show()

#Evaluate without PCA
data -= np.mean(data,axis=0)
data /= np.std(data,axis=0)

crossVal_scores = []
for train, test in kfold.split(data, label):
    crossVal_scores.append(KNNmodel(data[train],
        label[train],data[test],label[test]))

print('-----------NO PCA-------------')
print(crossVal_scores)
plt.ylabel('Cross Validation Accuracy')
plt.xlabel('Folds')
plt.plot([1,2,3,4,5],crossVal_scores,'ro')
plt.show()

print('Avarage Cross-Validation accuracy without PCA is %f' % (np.sum(crossVal_scores)/5))


